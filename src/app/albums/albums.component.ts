import { Component, OnInit, inject } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { observable } from 'rxjs';
import { Routes, RouterModule ,Router} from '@angular/router';
@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.css']
})
export class AlbumsComponent implements OnInit {
  albumid:any;
  userid:any;
  albums:any;
  constructor(private Http:HttpClient,private router:Router) { 
    this.userid=localStorage.getItem('userid');
    this.albumid=localStorage.getItem('albumid');
  }

  ngOnInit() {
    this.Http.get('https://jsonplaceholder.typicode.com/photos').subscribe(data=>{
      this.albums=data;

      console.log(JSON.stringify(this.albums));
      });

  }

}
