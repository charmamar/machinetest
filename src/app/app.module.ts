import { BrowserModule } from '@angular/platform-browser';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { HttpClientModule } from '@angular/common/http';
import { CommentsComponent } from './comments/comments.component';
import { AlbumsComponent } from './albums/albums.component'; 
// const routes: Routes = [
//   { path: 'details', component:UserDetailsComponent  }
// ];
const routes: Routes = [
  {path: '', component: UserListComponent },
  { path: 'details', component: UserDetailsComponent },
  { path: 'comments', component: CommentsComponent },
  { path: 'albums', component: AlbumsComponent },

];

@NgModule({
  declarations: [
    AppComponent,
    UserListComponent,
    UserDetailsComponent,
    CommentsComponent,
    AlbumsComponent
  ],
  imports: [
    RouterModule.forRoot(routes),
    BrowserModule,RouterModule,HttpClientModule
  ],
  exports: [
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
