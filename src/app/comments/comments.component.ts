import { Component, OnInit, inject } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { observable } from 'rxjs';
import { Routes, RouterModule ,Router} from '@angular/router';
@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {
  postid:any;
  userid:any;
  comments:any;
  constructor(private Http:HttpClient,private router:Router) { 
    this.userid=localStorage.getItem('userid');
    this.postid=localStorage.getItem('postid');
  }

  ngOnInit() {
    this.Http.get('https://jsonplaceholder.typicode.com/comments').subscribe(data=>{
      this.comments=data;

      console.log(JSON.stringify(this.comments));
      });

  }

}
