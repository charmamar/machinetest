import { Component, OnInit, inject } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { observable } from 'rxjs';
import { Routes, RouterModule ,Router} from '@angular/router';
@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {
  userpost:any;
  usercomment:any;
  useralbums:any;
  userid:any;
 // usercomment:any;
 todos:any;
  constructor(private Http:HttpClient,private router:Router) { 
    this.userid=localStorage.getItem('userid');
  }

  ngOnInit() {
    
    //For posts
    this.Http.get('https://jsonplaceholder.typicode.com/posts').subscribe(data=>{
      this.userpost=data;
      console.log(JSON.stringify(this.userpost));
      });
    //For Albums
    this.Http.get('https://jsonplaceholder.typicode.com/albums').subscribe(data=>{
      this.useralbums=data;

      console.log(JSON.stringify(this.useralbums));
      });
       //For TODO
    this.Http.get('https://jsonplaceholder.typicode.com/todos').subscribe(data=>{
      this.todos=data;

      console.log(JSON.stringify(this.todos));
      });
  }
oncommentshow(pid,userid){
  localStorage.setItem('postid',pid);
  localStorage.setItem('USER',userid);
  this.router.navigate(['comments']);
}
oncommentAlbums(aid,userid){
  localStorage.setItem('albumid',aid);
  localStorage.setItem('USER',userid);
  this.router.navigate(['albums']);
}

}
