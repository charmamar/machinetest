import { Component, OnInit, inject } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { observable } from 'rxjs';
import { Routes, RouterModule ,Router} from '@angular/router';
// import(inject) from 
@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  userlist:any;
  constructor(private Http:HttpClient,private router:Router) { }
 
  
  //https://jsonplaceholder.typicode.com/posts
  ngOnInit() {
    this.Http.get('https://jsonplaceholder.typicode.com/users').subscribe(data=>{
    this.userlist=data;
    console.log(JSON.stringify(this.userlist));
    });
  }

  userView(id){
    localStorage.setItem('userid',id);
    this.router.navigate(['details']);

  }

}
